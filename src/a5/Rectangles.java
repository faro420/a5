package a5;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Nico, G�tze> (<2125118>,<nico.goetze@haw-hamburg.de>),
 */
public class Rectangles {

    public static void main(String[] unused) {
        final int[][] r1 = { { -7, -9 }, { -3, -1 } };
        final int[][] r2 = { { -3, -1 }, { 1, 2 } };

        final int p1 = 0;
        final int p2 = 1;
        final int x = 0;
        final int y = 1;

        int r1minX, r1maxX, r1minY, r1maxY, r2minX, r2maxX, r2minY, r2maxY;

        if (r1[p1][x] < r1[p2][x]) {r1minX = r1[p1][x];r1maxX = r1[p2][x];} else {r1minX = r1[p2][x];r1maxX = r1[p1][x];}
        if (r1[p1][y] < r1[p2][y]) {r1minY = r1[p1][y];r1maxY = r1[p2][y];} else {r1minY = r1[p2][y];r1maxY = r1[p1][y];}
        if (r2[p1][x] < r2[p2][x]) {r2minX = r2[p1][x];r2maxX = r2[p2][x];} else {r2minX = r2[p2][x];r2maxX = r2[p1][x];}
        if (r2[p1][y] < r2[p2][y]) {r2minY = r2[p1][y];r2maxY = r2[p2][y];} else {r2minY = r2[p2][y];r2maxY = r2[p1][y];}

        boolean puffer = (r1maxX == r2minX || r1minX == r2maxX) && (r1maxY == r2minY || r1minY == r2maxY);

        if (r1maxX < r2minX || r1minX > r2maxX || r1maxY < r2minY || r1minY > r2maxY) {
            System.out.println("disjoint");
        }

        else if ((r1maxX == r2minX || r1minX == r2maxX || r1maxY == r2minY || r1minY == r2maxY) && puffer) {
            System.out.println("touching");
        } 
        
        else if ((r1maxX == r2minX || r1minX == r2maxX || r1maxY == r2minY || r1minY == r2maxY) && !puffer) {
            System.out.println("aligned");
        }

        else if (r1minX == r2minX && r1maxX == r2maxX && r1maxY == r2maxY && r1minY == r2minY) {
            System.out.println("same");
        }

        else if (r1minX >= r2minX && r1maxX <= r2maxX && r1maxY <= r2maxY && r1minY >= r2minY) {
            System.out.println("contained");
        }

        else {
            System.out.println("intersecting");
        }

    }
}
