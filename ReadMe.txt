Projekt:    a5, TI1-P1 SS14
Organisation:   HAW-Hamburg, Informatik
Team:       S1T5
Autor(en):
Baha, Mir Farshid   (mirfarshid.baha@haw-hamburg.de)
G�tze, Nico     (nico.goetze@haw-hamburg.de)

Review History
===========================

140422 v1.0 failed Schafers  bug 
int[][] r1 = { { -1, -1 }, { -10, -10 } };
int[][] r2 = { { -1, -5 }, {-5, -1} };

140429 v2.0 failed sch�fers
sauber auf
if (..){
   ....
}else if(... ){
   ....
}else if(... ){
   ....
}else if(... ){
   ....
}else[
   ....
}
umsetzen


140513 v3.0 ok Schafers

